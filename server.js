// server.js

// call the packages we need
let express     = require('express');   // call express
let app         = express();            // define our app using express
let bodyParser  = require('body-parser');
let mongoose    = require('mongoose');  // MongoDB connector
let apiRoutes   = require("./routes/routes");  // import routes

// set default port
let port = process.env.PORT || 8080;

// connect to db

mongoose.connect('mongodb+srv://marianahy:205459582@cluster0-pezv4.mongodb.net/test?retryWrites=true&w=majority',
    {
        useNewUrlParser: true,
        useUnifiedTopology: true 
    }
    );

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// REGISTER ROUTES
// all of our routes will be prefixed with /api
app.use('/api', apiRoutes);

// START THE SERVER
app.listen(port);
console.log('Listening to port: ' + port);