// routes.js

// Initialize express router
const express = require('express');
let router = express.Router();

// Set default API response (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'Welcome to the Library Reviews API' });   
});

module.exports = router